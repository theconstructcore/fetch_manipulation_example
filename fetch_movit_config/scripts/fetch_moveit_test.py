#! /usr/bin/env python

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group = moveit_commander.MoveGroupCommander("arm")
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)


pose_target = group.get_current_pose().pose
print("Current Pose=="+str(pose_target))

"""
pose_target = geometry_msgs.msg.Pose()

pose_target.orientation.x = 1.0
pose_target.orientation.y = 1.0
pose_target.orientation.z = 1.0
pose_target.orientation.w = 1.0
pose_target.position.x = 0.613384108189
pose_target.position.y = 4.49670146374e-05
pose_target.position.z = 1.11226310553

start
Current Pose==position:
  x: 0.613384108189
  y: 4.49670146374e-05
  z: 1.11226310553
orientation:
  x: 3.25440680462e-05
  y: -0.0213359655691
  z: 3.96562306998e-05
  w: 0.999772361061


 home:
 urrent Pose==position:
  x: 0.0550907161334
  y: -0.148985414991
  z: 0.565252830049
orientation:
  x: 0.477246546568
  y: -0.520836855408
  z: 0.495478126275
  w: 0.505436573885
"""

pose_target = geometry_msgs.msg.Pose()

pose_target.orientation.x = 3.25440680462e-05
pose_target.orientation.y = -0.0213359655691
pose_target.orientation.z = 3.96562306998e-05
pose_target.orientation.w = 0.999772361061
pose_target.position.x = 0.613384108189
pose_target.position.y = 4.49670146374e-05
pose_target.position.z = 1.11226310553

group.set_pose_target(pose_target)
print("Executing..START>"+str(pose_target.position))
plan1 = group.plan()
group.go(wait=True)
print("Executing..DONE>"+str(pose_target.position))

raw_input("Press Key to execute next movement....")


pose_target.orientation.x = 0.477246546568
pose_target.orientation.y = -0.520836855408
pose_target.orientation.z = 0.495478126275
pose_target.orientation.w = 0.505436573885
pose_target.position.x = 0.0550907161334
pose_target.position.y = -0.148985414991
pose_target.position.z = 0.565252830049

group.set_pose_target(pose_target)
print("Executing..HOME>"+str(pose_target.position))
plan1 = group.plan()
group.go(wait=True)
print("Executing..DONE>"+str(pose_target.position))


moveit_commander.roscpp_shutdown()